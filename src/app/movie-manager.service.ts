import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class MovieManagerService {
  private movies: Array<any> = [];
  private idKey: number = 0;

  constructor(private http: HttpClient) {
    // this.movies.push({
    //   id:this.getIdKey(),
    //   title:`${this.movies.length} - Morometii`,
    //   // linia de mai sus este echivalentul... --->>>  this.movies.length + " - Morometii "
    //   year: 2008,
    //   description: "description",
    //   director: "Director",
    // });
    // this.movies.push({
    //   id:this.getIdKey(),
    //   title:`${this.movies.length} - Morometii`,
    //   // linia de mai sus este echivalentul... --->>>  this.movies.length + " - Morometii "
    //   year: 2008,
    //   description: "description",
    //   director: "Director",
    // });
    // this.movies.push({
    //   id:this.getIdKey(),
    //   title:`${this.movies.length} - Morometii`,
    //   // linia de mai sus este echivalentul... --->>>  this.movies.length + " - Morometii "
    //   year: 2008,
    //   description: "description",
    //   director: "Director",
    // });
  }

  add(movie: any): any {
    // this.movies.push(movie);
    // return this.http.post(`${environment.baseUrl}/api/movie`, movie);
    let body = {
      title: movie.title,
      description: movie.description,
      year: movie.year,
      director: movie.director,
    }
    return this.http.post(`${environment.baseUrl}/api/movie`, body);
  }

  update(movie: any): any {
    let body = {
      id: movie.id,
      title: movie.title,
      description: movie.description,
      year: movie.year,
      director: movie.director,
    }
    // return this.http.post(`${environment.baseUrl}/api/movie`, body); // pentru a insera date - specific metodei add()
    return this.http.patch(`${environment.baseUrl}/api/movie/${movie.id}`, body); // pentru a actualiza date
  }

  delete(movie: any): any {
    // this.movies = this.movies.filter((item) => item.id != movie.id);
    return this.http.delete(`${environment.baseUrl}/api/movie/${movie.id}`);

  }

  get(): any {
    // return this.movies;
    return this.http.get(`${environment.baseUrl}/api/movie`);

  }

  getById(id: number): any {
    // let items = this.movies.filter((item) => item.id == id);
    // if (items.length == 1) {
    //   return items[0];
    // } else {
    //   return null;
    // }
    return this.http.get(`${environment.baseUrl}/api/movie/${id}`);
  }

  getIdKey(): number {
    return this.idKey += 1;
  }

}
