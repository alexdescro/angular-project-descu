import {Component, OnInit} from '@angular/core';
import {AuthServiceService} from "../auth-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  emailValue = "";
  passwordValue = "";
  repasswordValue = "";
  isShow = true;
  viewType: String = "login";
  // viewType: String = "";   pentru cazul de ngSwitch - default


  constructor(private authService: AuthServiceService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onLogin(): void {
    console.log(this.emailValue);
    console.log(this.passwordValue);
    // alert("Email: " + this.emailValue + ", password: " + this.passwordValue);
    this.authService.login(this.emailValue, this.passwordValue).subscribe((response) => {
      console.log(response);
      // this.router.navigate(["/dashboard"]);
      this.router.navigate(["dashboard"]);   // merge și fără / dinaintea lui dashboard
    });
  }

  onRegister(): void {
    console.log(this.emailValue);
    console.log(this.passwordValue);
    console.log(this.repasswordValue);
    // alert("Email: " + this.emailValue + ", password: " + this.passwordValue + ", repassword: " + this.repasswordValue);
    if (this.passwordValue == this.repasswordValue) {
      console.log("Passwords match");
      this.authService.register(this.emailValue, this.passwordValue).subscribe((response) => {
        console.log(response);
      });
    } else {
      console.log("Passwords do not match");
    }
  }

  onShow(): void {

    // console.log("before: " + this.isShow);
    // this.isShow = !this.isShow;
    // console.log("after: " + this.isShow);

    if (this.viewType == 'login') {
      this.viewType = "register";
    } else {
      this.viewType = "login";
    }
  }

}
